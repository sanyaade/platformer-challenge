#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMap>

#include <QMainWindow>
#include <QActionGroup>
#include <QTreeWidgetItem>

#include "editorscene.h"
#include "template.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void UpdateEntitiesList();

    void NewLevel();
    void LoadLevel(QString fileName);
    void SaveLevel(QString fileName);

    void SetCurrentLayer(int currentLayer);

private slots:

    void UpdatePropertiesGrid();
    void UpdateLevelBoundaries(QRectF boundaries);

    void OnModeChanged(QAction *modeAction);

    void on_actionOpen_triggered();
    void on_actionSaveAs_triggered();
    void on_actionSave_triggered();
    void on_actionNew_triggered();
    void on_actionExit_triggered();
    void on_actionBackground_Layer_triggered();
    void on_actionLevel_Layer_triggered();
    void on_actionForeground_Layer_triggered();
    void on_actionShowLayerColor_triggered();
    void on_actionTestLevel_triggered();
    void on_layerCombo_currentIndexChanged(int index);
    void on_zOrderSpin_editingFinished();
    void on_xSpin_editingFinished();
    void on_ySpin_editingFinished();
    void on_actionGridDuplication_triggered();
    void on_actionGridDuplicationMargin_triggered();
    void on_levelBoundY2Spin_valueChanged(int value);
    void on_levelBoundXSpin_valueChanged(int value);
    void on_levelBoundYSpin_valueChanged(int value);
    void on_levelBoundX2Spin_valueChanged(int value);

    void on_listWidget_itemClicked(QTreeWidgetItem *item, int column);

private:

    QActionGroup *modeActionsGroup;

    Ui::MainWindow *ui;
    EditorScene *scene;

    QString currentFileName;
};

#endif // MAINWINDOW_H
