#include "entity.h"

#include <QDebug>
#include <QGraphicsScene>

#include "editorscene.h"

Entity::Entity(Template *_templat, QGraphicsItem *parent) :
    QGraphicsItem(parent), templat(_templat), layer(0), zOrder(0), beginDragDrop(false)
{
    fileName = templat->entityFileName;
    name = templat->name;
    if(!QPixmapCache::find(templat->texturePath, &pixmap))
    {
        pixmap.load(templat->texturePath);
        QPixmapCache::insert(templat->texturePath, pixmap);
    }

    setFlags(QGraphicsItem::ItemIsMovable|QGraphicsItem::ItemIsFocusable|QGraphicsItem::ItemIsSelectable);
}

Entity* Entity::Clone()
{
    Entity *clonedItem = new Entity(templat, parentItem());

    clonedItem->setX(x());
    clonedItem->setY(y());
    clonedItem->zOrder = zOrder;
    clonedItem->layer = layer;

    scene()->addItem(clonedItem);
    UpdateZValue();

    return clonedItem;
}

QRectF Entity::boundingRect() const
{
    return QRectF(0, 0, pixmap.width(), pixmap.height());
}

void Entity::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->drawPixmap(0, 0, pixmap.width(), pixmap.height(), pixmap);

    if(qobject_cast<EditorScene*>(scene())->DoShowLayerColor())
    {
        painter->setPen(QPen(QColor(128, 128, 255, 0)));

        QColor layerColor;
        if(layer == -1)
            layerColor = QColor(0, 0, 255, 128);
        else if(layer == 0)
            layerColor = QColor(0, 255, 0, 128);
        else if(layer == 1)
            layerColor = QColor(255, 0, 0, 128);
        painter->setBrush(layerColor);

        painter->drawRect(QRect(0, 0, pixmap.width(), pixmap.height()));
    }
}

void Entity::mousePressEvent(QGraphicsSceneMouseEvent * event)
{
    QGraphicsItem::mousePressEvent(event);

    beginDragDrop = false;
    dragOffset = event->pos();
}

void Entity::mouseReleaseEvent(QGraphicsSceneMouseEvent * event)
{
    beginDragDrop = false;

    QGraphicsItem::mouseReleaseEvent(event);
}

void Entity::mouseMoveEvent(QGraphicsSceneMouseEvent * event)
{
    //Copy the entity at the beginnig of the drag&drop if Ctrl is pressed
    if(!beginDragDrop && (abs(event->scenePos().x() - event->lastScenePos().x()) > 0.f || abs(event->scenePos().y() - event->lastScenePos().y()) > 0.f))
    {
        beginDragDrop = true;
        originalPos = scenePos();

        if((event->modifiers() & Qt::ControlModifier) != 0)
        {
            //Copy all selected items
            for(int a = 0; a < scene()->selectedItems().size(); a++)
            {
                if(scene()->selectedItems().at(a)->type() != QGraphicsItem::UserType + 1)
                    continue;

                Entity *copiedEntity = qgraphicsitem_cast<Entity*>(scene()->selectedItems().at(a))->Clone();
                copiedEntity->UpdateZValue();
                copiedEntity->setPos(scene()->selectedItems().at(a)->scenePos());
            }
        }
    }

    QGraphicsItem::mouseMoveEvent(event);

    //The object has been moved enough to move it, restore its initial pos
    if((originalPos - scenePos()).manhattanLength() < 5)
    {
        setPos(originalPos);
        return;
    }

    //Recalculate the position on the grid
    float oldX = x();
    float oldY = y();

    //QPointF offset

    setX(qobject_cast<EditorScene*>(scene())->GetPositionOnGrid(QPointF(x(), y()) + dragOffset).x());
    setY(qobject_cast<EditorScene*>(scene())->GetPositionOnGrid(QPointF(x(), y()) + dragOffset).y());

    //Move all other selected items
    for(unsigned int a = 0; a < scene()->selectedItems().size(); a++)
    {
        if(scene()->selectedItems().at(a) == this) continue;

        scene()->selectedItems().at(a)->setX(scene()->selectedItems().at(a)->x() + (x() - oldX));
        scene()->selectedItems().at(a)->setY(scene()->selectedItems().at(a)->y() + (y() - oldY));
    }
}

void Entity::UpdateZValue()
{
    QString layerName;
    if(layer == -1)
        layerName = "Background";
    else if(layer == 0)
        layerName = "Level";
    else if(layer == 1)
        layerName = "Foreground";

    setToolTip("Layer: " + layerName + "\nZ-Order: " + QString::number(zOrder));

    setZValue(layer * 1000000.f + zOrder);
}
