#ifndef ENTITY_H
#define ENTITY_H

#include <QGraphicsItem>
#include <QPixmap>
#include <QPixmapCache>
#include <QPainter>
#include <QGraphicsSceneMouseEvent>

#include "template.h"

class Entity : public QGraphicsItem
{

public:
    explicit Entity(Template *_templat, QGraphicsItem *parent = 0);

    Entity* Clone();

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    void SetLayer(int _layer) {layer = _layer; UpdateZValue();}
    int GetLayer() const {return layer;}

    void SetZOrder(int _zOrder) {zOrder = _zOrder; UpdateZValue();}
    int GetZOrder() const {return zOrder;}

    int type() const { return UserType + 1; }

    QString fileName;
    QString name;

signals:

public slots:

protected:
    virtual void mousePressEvent(QGraphicsSceneMouseEvent * event);
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent * event);
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent * event);

private:
    void UpdateZValue();

    QPixmap pixmap;

    Template *templat;

    int layer;
    int zOrder;

    bool beginDragDrop;
    QPointF dragOffset;

    //Keep the old object position (before beginning of the drag&drop) to avoid unwanted moves
    QPointF originalPos;
};

#endif // ENTITY_H
