#ifndef PAUSESTATE_H
#define PAUSESTATE_H

#include "AbstractMenuState.h"

#include <SFML/Graphics/Texture.hpp>

class PauseState : public AbstractMenuState
{
public:
    PauseState();
    virtual ~PauseState();

    virtual void ProceedEvent(GameEngine *engine, sf::Event event);
    virtual void Update(GameEngine *engine, sf::Time timeDelta);
    virtual void Render(GameEngine *engine, sf::RenderTarget &target);

    virtual sf::Color GetBackgroundColor() const {return sf::Color(0, 0, 0);};

protected:

    virtual void OnItemSelection(GameEngine *engine, unsigned int item);

    virtual void OnPlay(GameEngine *engine);
    virtual void OnStop(GameEngine *engine);
    virtual void OnPause(GameEngine *engine);
    virtual void OnResume(GameEngine *engine);

private:
    sf::Texture background;
};

#endif //PAUSESTATE_H
