#ifndef STATEEVENT_H
#define STATEEVENT_H

#include <typeindex>
#include <typeinfo>

#include "entityx/entityx.h"

struct StateEvent : public entityx::Event<StateEvent>
{
    enum StateEventType
    {
        ChangeState,
        PushState,
        PopState
    };

    StateEvent(StateEventType type, std::type_index state) : type(type), state(state) {};

    StateEventType type;
    std::type_index state;
};

#endif // STATEEVENT_H
