#ifndef SOUNDEVENT_H
#define SOUNDEVENT_H

#include "entityx/entityx.h"

struct SoundEvent : public entityx::Event<SoundEvent>
{
    enum Action
    {
        Play,
        PlayIfNot,
        Stop
    };

    SoundEvent(entityx::Entity entity, const std::string &sound, Action action) : entity(entity), sound(sound), action(action) {};

    mutable entityx::Entity entity;
    std::string sound;
    Action action;
};

#endif // SOUNDEVENT_H
