#ifndef PHYSICEVENT_H
#define PHYSICEVENT_H

#include "entityx/entityx.h"

struct PhysicEvent : public entityx::Event<PhysicEvent>
{
    enum PhysicEventType
    {
        Nothing = 0,
        Walking,
        Idle,
        Jump,
        WatchingRight,
        WatchingLeft
    };

    PhysicEvent(entityx::Entity entity, PhysicEventType eventType) : entity(entity), eventType(eventType) {};

    mutable entityx::Entity entity;
    PhysicEventType eventType;
};

#endif // PHYSICEVENT_H
