#ifndef GAMEENGINE_H
#define GAMEENGINE_H

#include <iostream>
#include <map>
#include <stack>
#include <typeindex>
#include <typeinfo>
#include <unordered_map>

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Clock.hpp>

#include "GameState.h"

class GameEngine
{
    struct GameSharedData
    {
        std::string currentLevelPath;
        std::vector<int> players;
    };

public:
    GameEngine();
    ~GameEngine();

    void Init(int argc, char **argv);
    int Run();

    void ProcessEvents();
    void Update(sf::Time delta);
    void Draw();

    bool IsOpen() const;
    void Close();

    template<class T>
    void RegisterState();

    //Change of state.
    template<class T>
    void ChangeState();
    void ChangeState(std::type_index state);

    //Push a new state in front.
    template<class T>
    void PushState();
    void PushState(std::type_index state);

    //Pop the current state.
    void PopState();

    sf::RenderTarget& GetRenderTarget() {return target;};
    GameSharedData& GetSharedData() {return sharedData;};

private:
    sf::RenderWindow target;
    sf::Clock clock;

    std::stack<GameState*> currentStates;
    std::unordered_map<std::type_index, GameState*> states;

    GameSharedData sharedData;
};

template<class T>
void GameEngine::RegisterState()
{
    if(states.count(typeid(T)) == 0)
    {
        states[typeid(T)] = new T();
    }
}

template<class T>
void GameEngine::ChangeState()
{
    ChangeState(typeid(T));
}

template<class T>
void GameEngine::PushState()
{
    PushState(typeid(T));
}

#endif // GAMEENGINE_H
