#include "OptionsManager.h"

#include <iostream>
#include <sstream>

#include "Tools.h"

bool OptionsManager::InitFromArguments(int argc, char **argv)
{
    //Get all arguments
    std::vector<std::string> arguments;
    for(int i = 0; i < argc; i++)
    {
        arguments.push_back(std::string(argv[i]));
    }

    //Get all options
    for(int i = 0; i < argc; i++)
    {
        std::vector<std::string> parameter = tools::Split(arguments[i], "=");

        if(parameter[0].substr(0, 2) != "--")
            continue;

        std::string value;

        if(parameter[1].substr(0, 1) == "\"" && parameter[1].substr(parameter[1].size() - 2, 1) == "\"")
        {
            value = parameter[1].substr(1, parameter[1].size() - 2);
        }
        else
        {
            value = parameter[1];
        }

        options[parameter[0].substr(2, parameter[0].size() - 2)] = value;
    }

    return true;
}

bool OptionsManager::InitFromYAML(const std::string &optionPath)
{
    YAML::Node optionsFile = YAML::LoadFile(optionPath);
    InitFromNode(optionsFile, "");

    return true;
}

void OptionsManager::InitFromNode(const YAML::Node &node, const std::string &currentName)
{
    for(YAML::const_iterator it = node.begin(); it != node.end(); it++)
    {
        if(it->second.IsScalar())
        {
            options[currentName + "/" + it->first.as<std::string>()] = it->second.as<std::string>();
        }
        else
        {
            InitFromNode(it->second, currentName + "/" + it->first.as<std::string>());
        }
    }
}

const std::string& OptionsManager::GetOptionAsString(const std::string &name, const std::string &defaultValue)
{
    if(options.count(name) == 0)
        return defaultValue;

    return options[name];
}

const int OptionsManager::GetOptionAsInt(const std::string &name, const int defaultValue)
{
    if(options.count(name) == 0)
        return defaultValue;

    try
    {
        return std::stoi(options[name]);
    }
    catch(const std::invalid_argument& ia)
    {
        return defaultValue;
    }
}

const float OptionsManager::GetOptionAsFloat(const std::string &name, const float defaultValue)
{
    if(options.count(name) == 0)
        return defaultValue;

    try
    {
        return std::stof(options[name]);
    }
    catch(const std::invalid_argument& ia)
    {
        return defaultValue;
    }
}

const double OptionsManager::GetOptionAsDouble(const std::string &name, const double defaultValue)
{
    if(options.count(name) == 0)
        return defaultValue;

    try
    {
        return std::stod(options[name]);
    }
    catch(const std::invalid_argument& ia)
    {
        return defaultValue;
    }
}

const std::vector<int> OptionsManager::GetOptionAsIntVector(const std::string &name, const std::vector<int> defaultValue)
{
    std::vector<int> result;

    if(options.count(name) == 0)
        return defaultValue;

    std::string option = options[name];
    std::size_t pos = 0;

    while((pos = option.find(",")) != std::string::npos)
    {
        std::string token = option.substr(0, pos);

        option.erase(0, pos + 1);

        try
        {
            result.push_back(std::stoi(token));
        }
        catch(const std::invalid_argument& ia){}
    }

    try
    {
        result.push_back(std::stoi(option));
    }
    catch(const std::invalid_argument& ia){}

    return result;
}

bool OptionsManager::Exists(const std::string &name)
{
    return (options.count(name) != 0);
}
