#ifndef CLINEARMOVEMENT_H
#define CLINEARMOVEMENT_H

#include "entityx/entityx.h"

/**
Component : make an object move in a specified direction
 - Required by : no
 - Dependencies : no
*/
struct CLinearMovement : public entityx::Component<CLinearMovement>
{
    CLinearMovement(float speed = 0.f, int direction = 0) : speed(speed), direction(direction) {};

    float speed;
    int direction;
};

#endif // CLINEARMOVEMENT_H

