#ifndef COBSTACLE_H
#define COBSTACLE_H

#include "entityx/entityx.h"

/**
Component : make an object an obstacle for the physic engine
 - Required by : no
 - Dependencies : no
*/
struct CObstacle : public entityx::Component<CObstacle>
{
    enum PlatformType
    {
        Platform = 1,
        Jumpthru = 2,
        All = Platform|Jumpthru
    };

    CObstacle(PlatformType platformType = Platform, int layer = 0) : platformType(platformType), layer(layer) {};

    PlatformType platformType;
    int layer;
};

#endif // COBSTACLE_H

