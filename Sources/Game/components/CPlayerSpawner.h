#ifndef CPLAYERSPAWNER_H
#define CPLAYERSPAWNER_H

#include "entityx/entityx.h"

/**
Component : declare the entity as the player spawner (start point of the level)
 - Required by : no
 - Dependencies : no
*/
struct CPlayerSpawner : public entityx::Component<CPlayerSpawner>
{
    CPlayerSpawner() {};
};

#endif // CPLAYERSPAWNER_H
