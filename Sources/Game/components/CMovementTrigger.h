#ifndef CMOVEMENTTRIGGER_H
#define CMOVEMENTTRIGGER_H

#include "entityx/entityx.h"

struct CMovementTrigger : public entityx::Component<CMovementTrigger>
{
    CMovementTrigger(int direction = 0) : direction(direction) {};

    int direction;
};

#endif // CMOVEMENTTRIGGER_H
