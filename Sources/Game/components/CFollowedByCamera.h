#ifndef CFOLLOWEDBYCAMERA_H
#define CFOLLOWEDBYCAMERA_H

#include "entityx/entityx.h"

/**
Component : center the camera on the object
 - Required by : no
 - Dependencies : no
*/
struct CFollowedByCamera : public entityx::Component<CFollowedByCamera>
{

};

#endif
