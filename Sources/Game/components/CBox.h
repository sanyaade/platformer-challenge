#ifndef CBOX_H
#define CBOX_H

#include "entityx/entityx.h"

/**
Component : contains the position and the size
 - Required by : no
 - Dependencies : no
*/
struct CBox : public entityx::Component<CBox>
{
    CBox(float x = 0.f, float y = 0.f, float width = 0.f, float height = 0.f) : x(x), y(y), width(width), height(height) {};

    float x;
    float y;
    float width;
    float height;
};

#endif // CBOX_H
