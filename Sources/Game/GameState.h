#ifndef GAMESTATE_H
#define GAMESTATE_H

#include <map>

#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/System/Time.hpp>

class GameEngine;

class GameState
{
    public:

        enum State
        {
            Stopped,
            Pausing,
            Playing
        };

        GameState();
        virtual ~GameState();

        //World update and render functions
        virtual void ProceedEvent(GameEngine *engine, sf::Event event) = 0;
        virtual void Update(GameEngine *engine, sf::Time timeDelta) = 0;
        virtual void Render(GameEngine *engine, sf::RenderTarget &target) = 0;

        //State management functions
        void Play(GameEngine *engine);
        void Stop(GameEngine *engine);

        void Pause(GameEngine *engine);
        void Resume(GameEngine *engine);

        //Display properties
        virtual sf::Color GetBackgroundColor() const {return sf::Color(0, 0, 0, 255);};

    protected:

        virtual void OnPlay(GameEngine *engine);
        virtual void OnStop(GameEngine *engine);
        virtual void OnPause(GameEngine *engine);
        virtual void OnResume(GameEngine *engine);

    private:
        State currentState;
};

#endif // GAMESTATE_H
