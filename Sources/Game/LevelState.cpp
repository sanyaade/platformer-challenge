#include "LevelState.h"

#include "EntityFactory.h"
#include "GameEngine.h"
#include "PauseState.h"
#include "OptionsManager.h"
#include "Tools.h"
#include "components/CBox.h"
#include "components/CRender.h"
#include "components/CZOrder.h"
#include "components/CPolygon.h"
#include "components/CPlayer.h"
#include "components/CPlayerSpawner.h"
#include "components/CPhysic.h"
#include "components/CObstacle.h"
#include "events/StateEvent.h"
#include "systems/RenderSystem.h"
#include "systems/MovementSystem.h"
#include "systems/PolygonPositionerSystem.h"
#include "systems/PhysicSystem.h"
#include "systems/PlayerSystem.h"
#include "systems/EnemySystem.h"
#include "systems/HUDSystem.h"
#include "systems/SoundSystem.h"

#include "entityx/deps/Dependencies.h"

#include <Thor/Animation.hpp>

#include "yaml-cpp/yaml.h"

LevelState::LevelState() :
    GameState(),
    eventManager(),
    entityManager(),
    systemManager(),
    entityFactory(),
    levelSettings(),
    nextAction(Nothing),
    nextState(typeid(this))
{
}

LevelState::~LevelState()
{

}

void LevelState::ProceedEvent(GameEngine *engine, sf::Event event)
{
    if(event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)
    {
        engine->PushState<PauseState>();
    }
}

void LevelState::Update(GameEngine *engine, sf::Time timeDelta)
{
    if(nextAction == ChangeState)
    {
        engine->ChangeState(nextState);
    }
    else if(nextAction == PushState)
    {
        engine->PushState(nextState);
    }
    else if(nextAction == PopState)
    {
        engine->PopState();
    }

    systemManager->update<PolygonPositionerSystem>((double)timeDelta.asMicroseconds() / 1000000.d);
    systemManager->update<PlayerSystem>((double)timeDelta.asMicroseconds() / 1000000.d);
    systemManager->update<EnemySystem>((double)timeDelta.asMicroseconds() / 1000000.d);
    systemManager->update<MovementSystem>((double)timeDelta.asMicroseconds() / 1000000.d);
    systemManager->update<PhysicSystem>((double)timeDelta.asMicroseconds() / 1000000.d);
    systemManager->update<SoundSystem>((double)timeDelta.asMicroseconds() / 1000000.d);
    systemManager->update<RenderSystem>((double)timeDelta.asMicroseconds() / 1000000.d);
    systemManager->update<HUDSystem>((double)timeDelta.asMicroseconds() / 1000000.d);
}


void LevelState::Render(GameEngine *engine, sf::RenderTarget &target)
{

}

void LevelState::receive(const StateEvent &event)
{
    if(event.type == StateEvent::ChangeState)
    {
        nextAction = ChangeState;
        nextState = event.state;
    }
    if(event.type == StateEvent::PushState)
    {
        nextAction = PushState;
        nextState = event.state;
    }
    if(event.type == StateEvent::PopState)
    {
        nextAction = PopState;
    }
}

void LevelState::OnPlay(GameEngine *engine)
{
    eventManager.reset(new entityx::EventManager());
    entityManager.reset(new entityx::EntityManager(eventManager));
    systemManager.reset(new entityx::SystemManager(entityManager, eventManager));
    entityFactory.reset(new EntityFactory(entityManager));

    systemManager->add<entityx::deps::Dependency<CRender, CZOrder>>();
    systemManager->add<RenderSystem>(engine->GetRenderTarget(), levelSettings);
    systemManager->add<PolygonPositionerSystem>();
    systemManager->add<PhysicSystem>();
    systemManager->add<PlayerSystem>(levelSettings);
    systemManager->add<MovementSystem>();
    systemManager->add<EnemySystem>();
    systemManager->add<HUDSystem>(engine->GetRenderTarget());
    systemManager->add<SoundSystem>();

    //Configure
    nextAction = Nothing;
    systemManager->configure();
    eventManager->subscribe<StateEvent>(*this);

    std::cout << std::endl << "Entityx: EventManager and EntityManager initialized." << std::endl;

    //Load the level and create player on the player spawner
    LoadLevel(engine->GetSharedData().currentLevelPath);
    CreatePlayers(engine->GetSharedData().players);

    //Update to force resources loading
    systemManager->update<RenderSystem>(0.00);
    systemManager->update<HUDSystem>(0.00);
}

void LevelState::OnStop(GameEngine *engine)
{

}

void LevelState::OnPause(GameEngine *engine)
{

}

void LevelState::OnResume(GameEngine *engine)
{

}

void LevelState::LoadLevel(std::string levelPath)
{
    YAML::Node lvlNode;
    try
    {
        lvlNode = YAML::LoadFile(levelPath);
    }
    catch(const YAML::BadFile &badFile)
    {
        std::cout << "Can't load the level \"" << levelPath << "\"" << std::endl;
    }

    if(lvlNode["levelBoundaries"])
    {
        levelSettings.levelBoundaries.left = lvlNode["levelBoundaries"]["x"].as<float>();
        levelSettings.levelBoundaries.top = lvlNode["levelBoundaries"]["y"].as<float>();
        levelSettings.levelBoundaries.width = lvlNode["levelBoundaries"]["x2"].as<float>() - lvlNode["levelBoundaries"]["x"].as<float>();
        levelSettings.levelBoundaries.height = lvlNode["levelBoundaries"]["y2"].as<float>() - lvlNode["levelBoundaries"]["y"].as<float>();
    }

    if(lvlNode["entities"])
    {
        for(YAML::const_iterator it = lvlNode["entities"].begin(); it != lvlNode["entities"].end(); it++)
        {
            if((*it)["template"] && (*it)["name"] && (*it)["components"])
            {
                entityx::Entity newEntity = entityManager->create();
                entityFactory->CreateEntityFromTemplate(newEntity, (*it)["template"].as<std::string>(), (*it)["name"].as<std::string>());
                entityFactory->CreateEntityFromSave(newEntity, (*it));
            }
        }
    }
}

void LevelState::CreatePlayers(std::vector<int> playerIds)
{
    std::cout << "LevelState: Creating players..." << std::endl;

    for(auto spawner : entityManager->entities_with_components<CPlayerSpawner>())
    {
        entityx::ptr<CBox> cbox = spawner.component<CBox>();

        for(unsigned int a = 0; a < playerIds.size(); a++)
        {
            std::cout << "LevelState: Creating player ID:" << playerIds[a] << " as player #" << a + 1 << std::endl;

            entityx::Entity newPlayer = entityManager->create();
            entityFactory->CreateEntityFromTemplate(newPlayer, "templates/players/player" + std::to_string(playerIds[a]) + ".entity", "Player" + std::to_string(playerIds[a]));

            entityx::ptr<CBox> playerPos = newPlayer.component<CBox>();
            playerPos->x = cbox->x + a * 50;
            playerPos->y = cbox->y;

            //Set the player number
            entityx::ptr<CPlayer> playerProp = newPlayer.component<CPlayer>();
            playerProp->playerNumber = a + 1;

            //Set the correct keys
            playerProp->leftKey = static_cast<sf::Keyboard::Key>(OptionsManager::Instance().GetOptionAsInt("/keyboardMapping/player" + std::to_string(a + 1) + "/left", -1));
            playerProp->rightKey = static_cast<sf::Keyboard::Key>(OptionsManager::Instance().GetOptionAsInt("/keyboardMapping/player" + std::to_string(a + 1) + "/right", -1));
            playerProp->jumpKey = static_cast<sf::Keyboard::Key>(OptionsManager::Instance().GetOptionAsInt("/keyboardMapping/player" + std::to_string(a + 1) + "/jump", -1));
        }
    }
}
