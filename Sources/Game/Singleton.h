#ifndef SINGLETON_H
#define SINGLETON_H

template<typename T> class Singleton
{
private:

public:
    static T& Instance()
    {
        static T theSingleInstance;
        return theSingleInstance;
    }
};

#endif // SINGLETON_H
