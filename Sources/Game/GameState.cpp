#include "GameState.h"

#include "GameEngine.h"

GameState::GameState() : currentState(Stopped)
{
    //ctor
}

GameState::~GameState()
{
    //dtor
}

void GameState::Play(GameEngine *engine)
{
    if(currentState != Stopped)
        OnStop(engine);
    OnPlay(engine);
    currentState = Playing;
}

void GameState::Stop(GameEngine *engine)
{
    OnStop(engine);
    currentState = Stopped;
}

void GameState::Pause(GameEngine *engine)
{
    if(currentState == Playing)
    {
        OnPause(engine);
        currentState = Pausing;
    }
}

void GameState::Resume(GameEngine *engine)
{
    if(currentState == Pausing)
    {
        OnResume(engine);
        currentState = Playing;
    }
}

void GameState::OnPlay(GameEngine *engine)
{

}

void GameState::OnStop(GameEngine *engine)
{

}

void GameState::OnPause(GameEngine *engine)
{

}

void GameState::OnResume(GameEngine *engine)
{

}
