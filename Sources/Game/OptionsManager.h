#ifndef OPTIONSMANAGER_H
#define OPTIONSMANAGER_H

#include <string>
#include <vector>
#include <map>
#include "yaml-cpp/yaml.h"

#include "Singleton.h"

class OptionsManager : public Singleton<OptionsManager>
{
    friend class Singleton<OptionsManager>;

public:
    bool InitFromArguments(int argc, char **argv);
    bool InitFromYAML(const std::string &optionPath);

    //Options getters
    const std::string& GetOptionAsString(const std::string &name, const std::string &defaultValue);

    const int GetOptionAsInt(const std::string &name, const int defaultValue);
    const float GetOptionAsFloat(const std::string &name, const float defaultValue);
    const double GetOptionAsDouble(const std::string &name, const double defaultValue);

    const std::vector<int> GetOptionAsIntVector(const std::string &name, const std::vector<int> defaultValue);

    bool Exists(const std::string &name);

private:
    void InitFromNode(const YAML::Node &node, const std::string &currentName);

    std::map<std::string, std::string> options;
};


#endif // OPTIONSMANAGER_H
