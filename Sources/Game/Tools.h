#ifndef TOOLS_H
#define TOOLS_H

#include <string>
#include <sstream>
#include <vector>

namespace tools
{
    inline std::vector<std::string> Split(const std::string &str, const std::string &delimiter)
    {
        std::vector<std::string> result;

        std::string tokens = str;
        std::size_t pos = 0;

        while((pos = tokens.find(delimiter)) != std::string::npos)
        {
            std::string token = tokens.substr(0, pos);
            tokens.erase(0, pos + delimiter.size());
            result.push_back(token);
        }
        result.push_back(tokens);

        return result;
    }

    inline std::vector<int> SplitToInt(const std::string &str, const std::string &delimiter)
    {
        std::vector<int> result;

        std::string tokens = str;
        std::size_t pos = 0;

        while((pos = tokens.find(delimiter)) != std::string::npos)
        {
            std::string token = tokens.substr(0, pos);
            tokens.erase(0, pos + delimiter.size());
            result.push_back(std::stoi(token));
        }
        result.push_back(std::stoi(tokens));

        return result;
    }
}

#endif // TOOLS_H
