#include "GameEngine.h"

#include "GameState.h"
#include "OptionsManager.h"
#include "Tools.h"

GameEngine::GameEngine() : target(), clock(), currentStates()
{

}

GameEngine::~GameEngine()
{

}

void GameEngine::Init(int argc, char **argv)
{
    //Create a window
    target.create(sf::VideoMode(1024, 768), "Platformer Challenge");
    target.setVerticalSyncEnabled(true);

    //Load arguments and option file
    OptionsManager::Instance().InitFromArguments(argc, argv);
    OptionsManager::Instance().InitFromYAML("options.yaml");

    //Load GameSharedData
    sharedData.currentLevelPath = OptionsManager::Instance().GetOptionAsString("level", "");
    sharedData.players = tools::SplitToInt(OptionsManager::Instance().GetOptionAsString("players", "1"), ",");
}

int GameEngine::Run()
{
    clock.restart();

    while(IsOpen())
    {
        ProcessEvents();

        if(!currentStates.empty())
            target.clear(currentStates.top()->GetBackgroundColor());
        else
            target.clear();

        Update(clock.restart());
        Draw();

        target.display();
    }

    return 0;
}

void GameEngine::ProcessEvents()
{
    if(currentStates.empty())
        return;

    sf::Event event;
    while(target.pollEvent(event))
    {
        if(event.type == sf::Event::Closed)
            target.close();
        currentStates.top()->ProceedEvent(this, event);
    }
}

void GameEngine::Update(sf::Time delta)
{
    if(currentStates.empty())
        return;

    currentStates.top()->Update(this, delta);
}

void GameEngine::Draw()
{
    if(currentStates.empty())
        return;

    currentStates.top()->Render(this, target);
}

bool GameEngine::IsOpen() const
{
    return target.isOpen();
}

void GameEngine::Close()
{
    while(!currentStates.empty())
    {
        currentStates.top()->Stop(this);
        currentStates.pop();
    }

    target.close();
}

void GameEngine::ChangeState(std::type_index state)
{
    while(!currentStates.empty())
    {
        currentStates.top()->Stop(this);
        currentStates.pop();
    }

    currentStates.push(states[state]);
    currentStates.top()->Play(this);

    clock.restart();

    std::cout << "GameEngine: Switched to " << state.name() << " state." << std::endl;
}

void GameEngine::PushState(std::type_index state)
{
    currentStates.top()->Pause(this);

    currentStates.push(states[state]);
    currentStates.top()->Play(this);

    clock.restart();

    std::cout << "GameEngine: Pushed " << state.name() << " state in front." << std::endl;
}

void GameEngine::PopState()
{
    if(currentStates.size() < 2)
        return;

    currentStates.top()->Stop(this);
    currentStates.pop();

    currentStates.top()->Resume(this);

    clock.restart();

    std::cout << "GameEngine: Popped current state" << std::endl;
}
