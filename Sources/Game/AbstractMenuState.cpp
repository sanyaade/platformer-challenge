#include "AbstractMenuState.h"

#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Window/Event.hpp>

#include "resources/Resources.h"

AbstractMenuState::AbstractMenuState(const std::string &_menuTitle, const std::vector<std::string>& _items, unsigned int _currentItem) :
    menuTitle(_menuTitle),
    items(_items),
    currentItem(_currentItem),
    buttonSound(),
    validSound()
{
    if(!Resources::Instance().GetFontHolder().IsLoaded("res/kenpixel.ttf"))
        Resources::Instance().GetFontHolder().Load("res/kenpixel.ttf");

    if(!Resources::Instance().GetSoundHolder().IsLoaded("res/Sound/menuButton.wav"))
        Resources::Instance().GetSoundHolder().Load("res/Sound/menuButton.wav");
    if(!Resources::Instance().GetSoundHolder().IsLoaded("res/Sound/menuButtonValid.wav"))
        Resources::Instance().GetSoundHolder().Load("res/Sound/menuButtonValid.wav");

    buttonSound.setRelativeToListener(true);
    validSound.setRelativeToListener(true);

    buttonSound.setBuffer(Resources::Instance().GetSoundHolder().Get("res/Sound/menuButton.wav"));
    validSound.setBuffer(Resources::Instance().GetSoundHolder().Get("res/Sound/menuButtonValid.wav"));
}

AbstractMenuState::~AbstractMenuState()
{

}

void AbstractMenuState::ProceedEvent(GameEngine *engine, sf::Event event)
{
    if(event.type == sf::Event::KeyPressed)
    {
        if(event.key.code == sf::Keyboard::Up)
        {
            buttonSound.play();

            if(currentItem > 0)
                currentItem--;

            if(items[currentItem] == "##SEPARATOR##")
            {
                if(currentItem > 0)
                    currentItem--;
                else if(currentItem < items.size() - 1)
                    currentItem++;
            }
        }
        else if(event.key.code == sf::Keyboard::Down)
        {
            buttonSound.play();

            if(currentItem < items.size() - 1)
                currentItem++;

            if(items[currentItem] == "##SEPARATOR##")
            {
                if(currentItem < items.size() - 1)
                    currentItem++;
                else if(currentItem > 0)
                    currentItem--;
            }
        }
        else if(event.key.code == sf::Keyboard::Return)
        {
            validSound.play();
            OnItemSelection(engine, currentItem);
        }
    }
}

void AbstractMenuState::Update(GameEngine *engine, sf::Time timeDelta)
{

}

void AbstractMenuState::Render(GameEngine *engine, sf::RenderTarget &target)
{
    //Draw menu title
    sf::Text menuTitleTxt(menuTitle, Resources::Instance().GetFontHolder().Get("res/kenpixel.ttf"), 48);
    menuTitleTxt.setPosition(1024.f/2 - menuTitleTxt.getLocalBounds().width/2.f, 200.f);
    target.draw(menuTitleTxt);

    //Draw items
    sf::Text itemText("", Resources::Instance().GetFontHolder().Get("res/kenpixel.ttf"), 32);
    itemText.setPosition(1024.f/2 - menuTitleTxt.getLocalBounds().width/2.f, 260.f);

    for(unsigned int a = 0; a < items.size(); a++)
    {
        float heightOffset = (a == 0 ? 40.f : itemText.getGlobalBounds().height + 20.f);

        if(currentItem == a)
            itemText.setColor(sf::Color(200, 200, 0));
        else
            itemText.setColor(sf::Color(255, 255, 255));
        itemText.setString(items[a]);
        itemText.move(0.f, heightOffset);

        if(items[a] == "##SEPARATOR##")
            continue;

        target.draw(itemText);
    }
}

void AbstractMenuState::OnPlay(GameEngine *engine)
{
    currentItem = 0;
}

void AbstractMenuState::OnStop(GameEngine *engine)
{

}

void AbstractMenuState::OnPause(GameEngine *engine)
{

}

void AbstractMenuState::OnResume(GameEngine *engine)
{

}
