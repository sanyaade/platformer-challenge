#ifndef ENEMYSYSTEM_H
#define ENEMYSYSTEM_H

#include "entityx/entityx.h"

/**
System : manage enemies
 - Components used : CBox, CPolygon, CPhysic, CEnemy, CMovementTrigger
*/
class EnemySystem : public entityx::System<EnemySystem>, public entityx::Receiver<EnemySystem>
{
public:
    EnemySystem();

    void configure(entityx::ptr<entityx::EventManager> eventManager);
    void update(entityx::ptr<entityx::EntityManager> es, entityx::ptr<entityx::EventManager> events, double dt);
};

#endif // ENEMYSYSTEM_H
