#include "HUDSystem.h"

#include <iostream>

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Sprite.hpp>

#include "../components/CPlayer.h"
#include "../resources/Resources.h"

#define MAX_HEALTH 6

HUDSystem::HUDSystem(sf::RenderTarget &renderTarget) : target(renderTarget)
{

}

void HUDSystem::configure(entityx::ptr<entityx::EventManager> eventManager)
{

}

void HUDSystem::update(entityx::ptr<entityx::EntityManager> es, entityx::ptr<entityx::EventManager> events, double dt)
{
    if(!Resources::Instance().GetTextureHolder().IsLoaded("res/HUD/hud_heartEmpty.png"))
        Resources::Instance().GetTextureHolder().Load("res/HUD/hud_heartEmpty.png");
    if(!Resources::Instance().GetTextureHolder().IsLoaded("res/HUD/hud_heartHalf.png"))
        Resources::Instance().GetTextureHolder().Load("res/HUD/hud_heartHalf.png");
    if(!Resources::Instance().GetTextureHolder().IsLoaded("res/HUD/hud_heartFull.png"))
        Resources::Instance().GetTextureHolder().Load("res/HUD/hud_heartFull.png");

    sf::Sprite emptyHeart(Resources::Instance().GetTextureHolder().Get("res/HUD/hud_heartEmpty.png"));
    sf::Sprite halfHeart(Resources::Instance().GetTextureHolder().Get("res/HUD/hud_heartHalf.png"));
    sf::Sprite fullHeart(Resources::Instance().GetTextureHolder().Get("res/HUD/hud_heartFull.png"));

    for(auto entity : es->entities_with_components<CPlayer>())
    {
        entityx::ptr<CPlayer> cplayer = entity.component<CPlayer>();

        if(!Resources::Instance().GetTextureHolder().IsLoaded(cplayer->playerIcon))
            Resources::Instance().GetTextureHolder().Load(cplayer->playerIcon);

        sf::Sprite playerIcon(Resources::Instance().GetTextureHolder().Get(cplayer->playerIcon));
        sf::Vector2f heartsIniPos(24.f + (1000.f/4.f) * (cplayer->playerNumber - 1), 16.f);

        playerIcon.setPosition(heartsIniPos);
        target.draw(playerIcon);

        for(int a = 0; a < MAX_HEALTH/2; a++)
        {
            sf::Vector2f heartPos(heartsIniPos + sf::Vector2f(55.f + a * 30.f, 0));
            int currentHeartContent = cplayer->health - 2*a;

            if(currentHeartContent <= 0)
            {
                //This heart is empty
                emptyHeart.setPosition(heartPos);
                target.draw(emptyHeart);
            }
            else if(currentHeartContent == 1)
            {
                //This heart is half empty
                halfHeart.setPosition(heartPos);
                target.draw(halfHeart);
            }
            else
            {
                //This heart is full
                fullHeart.setPosition(heartPos);
                target.draw(fullHeart);
            }
        }
    }
}
