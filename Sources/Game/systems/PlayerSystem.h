#ifndef PlayerSystem_H
#define PlayerSystem_H

#include <set>

#include "entityx/entityx.h"

class PlayerHealthEvent;

struct LevelSettings;

/**
System : set if the object will move according to the keyboard input
 - Components used : CKeyboard
 - Send events : MoveEvent
*/
class PlayerSystem : public entityx::System<PlayerSystem>, public entityx::Receiver<PlayerSystem>
{
public:
    PlayerSystem(LevelSettings &levelSettings);

    void configure(entityx::ptr<entityx::EventManager> eventManager);

    void update(entityx::ptr<entityx::EntityManager> es, entityx::ptr<entityx::EventManager> events, double dt);

    void receive(const PlayerHealthEvent& event);

private:
    LevelSettings &settings;
};

#endif // PlayerSystem_H
