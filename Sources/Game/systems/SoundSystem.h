#ifndef SOUNDSYSTEM_H
#define SOUNDSYSTEM_H

#include "entityx/entityx.h"

class CSoundSource;
class SoundEvent;

namespace sf
{
    class Sound;
}

/**
System : plays and manages sounds
 - Components used : -
*/
class SoundSystem : public entityx::System<SoundSystem>, public entityx::Receiver<SoundSystem>
{
    struct IsStopped
    {
        bool operator() (const sf::Sound &sound);
    };

public:
    SoundSystem();

    void configure(entityx::ptr<entityx::EventManager> eventManager);

    void update(entityx::ptr<entityx::EntityManager> es, entityx::ptr<entityx::EventManager> events, double dt);

    void receive(const SoundEvent &event);

private:
    void CreateNewSound(entityx::ptr<CSoundSource> csound, const SoundEvent &event);
};

#endif // SOUNDSYSTEM_H
