#include "PlayerSystem.h"

#include <iostream>

#include <SFML/Window/Keyboard.hpp>

#include "../LevelState.h"
#include "../MainMenu.h"
#include "../components/CBox.h"
#include "../components/CFinishLine.h"
#include "../components/CPlayer.h"
#include "../components/CPhysic.h"
#include "../components/CPolygon.h"
#include "../components/CRender.h"
#include "../events/PlayerHealthEvent.h"
#include "../events/MoveEvent.h"
#include "../events/StateEvent.h"

PlayerSystem::PlayerSystem(LevelSettings &levelSettings) : settings(levelSettings)
{

}

void PlayerSystem::configure(entityx::ptr<entityx::EventManager> eventManager)
{
    eventManager->subscribe<PlayerHealthEvent>(*this);
}

void PlayerSystem::update(entityx::ptr<entityx::EntityManager> es, entityx::ptr<entityx::EventManager> events, double dt)
{
    std::vector<entityx::Entity> playerToDestroy;
    unsigned int playerCount(0), finishedPlayerCount(0);

    //Manage players
    for(auto entity : es->entities_with_components<CPlayer>())
    {
        entityx::ptr<CPlayer> cplayerComp = entity.component<CPlayer>();
        entityx::ptr<CPolygon> cpolygonComp = entity.component<CPolygon>();
        entityx::ptr<CBox> cboxComp = entity.component<CBox>();
        entityx::ptr<CRender> crenderComp = entity.component<CRender>();

        playerCount++;
        if(cplayerComp->hasFinished)
        {
            finishedPlayerCount++;
        }
        else
        {
            //Test collision between the player and the finish lines.
            for(auto finishLines : es->entities_with_components<CFinishLine, CPolygon>())
            {
                entityx::ptr<CPolygon> cpolyFinish = finishLines.component<CPolygon>();

                if(PolygonCollision(cpolygonComp->polygon, cpolyFinish->polygon))
                {
                    cplayerComp->hasFinished = true;
                    events->emit<MoveEvent>(entity, MoveEvent::Jump);
                }
            }

            //Move the player
            if(sf::Keyboard::isKeyPressed(cplayerComp->jumpKey))
            {
                events->emit<MoveEvent>(entity, MoveEvent::Jump);
            }
            if(sf::Keyboard::isKeyPressed(cplayerComp->leftKey))
            {
                events->emit<MoveEvent>(entity, MoveEvent::GoLeft);
            }
            if(sf::Keyboard::isKeyPressed(cplayerComp->rightKey))
            {
                events->emit<MoveEvent>(entity, MoveEvent::GoRight);
            }

            //Change the player opacity if the player was hit
            if(crenderComp)
            {
                if(cplayerComp->invincibilityTimer.getElapsedTime().asSeconds() < 2.f)
                {
                    crenderComp->sprite.setColor(sf::Color(255, 255, 255, 128));
                }
                else
                {
                    crenderComp->sprite.setColor(sf::Color(255, 255, 255, 255));
                }
            }

            //Kill the player if he is under the level boundaries
            if(cboxComp->y > settings.levelBoundaries.top + settings.levelBoundaries.height)
            {
                events->emit<PlayerHealthEvent>(entity, PlayerHealthEvent::LoseAllPV);
            }
            //Block if he goes on the side of boundaries
            else if(cboxComp->x < settings.levelBoundaries.left)
            {
                cboxComp->x = settings.levelBoundaries.left;
            }
            else if(cboxComp->x > settings.levelBoundaries.left + settings.levelBoundaries.width)
            {
                cboxComp->x = settings.levelBoundaries.left + settings.levelBoundaries.width;
            }

            //Delete the player if its health is < 0
            if(cplayerComp->health <= 0)
            {
                playerToDestroy.push_back(entity);
            }
        }
    }

    for(auto entity : playerToDestroy)
    {
        entity.destroy();
    }

    //Go to LevelEndState when all players have finished the level
    if(playerCount == finishedPlayerCount)
    {
        events->emit<StateEvent>(StateEvent::ChangeState, typeid(MainMenu));
    }

    //Go to the LevelFailureState when all players have been killed
    if(playerCount == 0)
    {
        events->emit<StateEvent>(StateEvent::ChangeState, typeid(LevelState));
    }
}

void PlayerSystem::receive(const PlayerHealthEvent& event)
{
    if(!event.entity.component<CPlayer>())
        return;

    entityx::ptr<CPlayer> cplayer = event.entity.component<CPlayer>();

    switch (event.eventType)
    {

    case PlayerHealthEvent::LosePV:
        cplayer->health--;
        break;

    case PlayerHealthEvent::LoseAllPV:
        cplayer->health = 0;
        break;

    case PlayerHealthEvent::GainPV:
        if(cplayer->health < 6)
            cplayer->health++;
        break;

    case PlayerHealthEvent::GainAllPV:
        cplayer->health = 6;
        break;

    default:
        break;

    }
}
