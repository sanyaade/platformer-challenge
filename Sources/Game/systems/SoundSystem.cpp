#include "SoundSystem.h"

#include <SFML/Audio/Sound.hpp>
#include <SFML/Audio/SoundBuffer.hpp>

#include "../components/CBox.h"
#include "../components/CSoundSource.h"
#include "../events/SoundEvent.h"
#include "../resources/Resources.h"


bool SoundSystem::IsStopped::operator() (const sf::Sound &sound)
{
    return sound.getStatus() == sf::Sound::Stopped;
}

SoundSystem::SoundSystem()
{

}

void SoundSystem::configure(entityx::ptr<entityx::EventManager> eventManager)
{
    eventManager->subscribe<SoundEvent>(*this);
}

void SoundSystem::update(entityx::ptr<entityx::EntityManager> es, entityx::ptr<entityx::EventManager> events, double dt)
{
    //Move the sound alongside with the entity
    for(auto entity : es->entities_with_components<CSoundSource, CBox>())
    {
        entityx::ptr<CBox> cbox = entity.component<CBox>();
        entityx::ptr<CSoundSource> csound = entity.component<CSoundSource>();

        for(auto it = csound->playingSounds.begin(); it != csound->playingSounds.end(); it++)
        {
            it->second.setPosition((int)cbox->x, (int)cbox->y, 0);
        }
    }
}

void SoundSystem::receive(const SoundEvent &event)
{
    entityx::ptr<CSoundSource> csound = event.entity.component<CSoundSource>();

    //Ignore if the entity doesn't have a CSoundSource
    if(!csound)
        return;

    //Ignore if the sound doesn't exist
    if(csound->listOfSounds.count(event.sound) == 0)
        return;

    //Play the sound
    if(event.action == SoundEvent::Play || event.action == SoundEvent::PlayIfNot)
    {
        if(csound->playingSounds.count(event.sound) == 0)
        {
            CreateNewSound(csound, event);
            csound->playingSounds[event.sound].play();
        }
        else if(event.action == SoundEvent::Play || (event.action == SoundEvent::PlayIfNot && csound->playingSounds[event.sound].getStatus() == sf::Sound::Stopped))
        {
            csound->playingSounds[event.sound].play();
        }
    }
    else if(event.action == SoundEvent::Stop)
    {
        if(csound->playingSounds.count(event.sound) == 1 && csound->playingSounds[event.sound].getStatus() != sf::Sound::Stopped)
        {
            csound->playingSounds[event.sound].stop();
        }
    }
}

void SoundSystem::CreateNewSound(entityx::ptr<CSoundSource> csound, const SoundEvent &event)
{
    if(!Resources::Instance().GetSoundHolder().IsLoaded(csound->listOfSounds[event.sound].path))
        Resources::Instance().GetSoundHolder().Load(csound->listOfSounds[event.sound].path);

    csound->playingSounds[event.sound] = sf::Sound(Resources::Instance().GetSoundHolder()
                                                   .Get(csound->listOfSounds[event.sound].path));
    csound->playingSounds[event.sound].setMinDistance(200);
    csound->playingSounds[event.sound].setLoop(csound->listOfSounds[event.sound].repeat);
    csound->playingSounds[event.sound].setVolume(csound->listOfSounds[event.sound].volume);
}
