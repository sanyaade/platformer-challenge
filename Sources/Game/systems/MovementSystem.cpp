#include "MovementSystem.h"

#include "../components/CBox.h"
#include "../components/CEnemy.h"
#include "../components/CPolygon.h"
#include "../components/CPhysic.h"
#include "../components/CLinearMovement.h"
#include "../components/CMovementTrigger.h"
#include "../events/MoveEvent.h"

#include "../polygon.h"

MovementSystem::MovementSystem()
{

}

void MovementSystem::configure(entityx::ptr<entityx::EventManager> eventManager)
{

}

void MovementSystem::update(entityx::ptr<entityx::EntityManager> es, entityx::ptr<entityx::EventManager> events, double dt)
{
    //Update entities with CLinearMovement
    for(auto entity : es->entities_with_components<CLinearMovement, CBox, CPolygon>())
    {
        entityx::ptr<CBox> cboxComp = entity.component<CBox>();
        entityx::ptr<CPolygon> cpolyComp = entity.component<CPolygon>();
        entityx::ptr<CLinearMovement> clinearMovComp = entity.component<CLinearMovement>();

        for(auto trigger : es->entities_with_components<CMovementTrigger, CBox, CPolygon>())
        {
            entityx::ptr<CPolygon> cpolyTrig = trigger.component<CPolygon>();
            entityx::ptr<CMovementTrigger> ctrigger = trigger.component<CMovementTrigger>();

            if(PolygonCollision(cpolyComp->polygon, cpolyTrig->polygon))
            {
                clinearMovComp->direction = ctrigger->direction;
            }
        }

        if(clinearMovComp->direction == 0)
        {
            cboxComp->x += clinearMovComp->speed * dt;
        }
        else if(clinearMovComp->direction == 1)
        {
            cboxComp->y += clinearMovComp->speed * dt;
        }
        else if(clinearMovComp->direction == 2)
        {
            cboxComp->x -= clinearMovComp->speed * dt;
        }
        else if(clinearMovComp->direction == 3)
        {
            cboxComp->y -= clinearMovComp->speed * dt;
        }
    }

    //Update enemies with CPhysic and CEnemy component
    for(auto entity : es->entities_with_components<CEnemy, CPhysic, CBox, CPolygon>())
    {
        entityx::ptr<CBox> cboxComp = entity.component<CBox>();
        entityx::ptr<CPolygon> cpolyComp = entity.component<CPolygon>();
        entityx::ptr<CEnemy> cenemyComp = entity.component<CEnemy>();

        //Detect triggers
        for(auto trigger : es->entities_with_components<CMovementTrigger, CBox, CPolygon>())
        {
            entityx::ptr<CPolygon> cpolyTrig = trigger.component<CPolygon>();
            entityx::ptr<CMovementTrigger> ctrigger = trigger.component<CMovementTrigger>();

            if(PolygonCollision(cpolyComp->polygon, cpolyTrig->polygon))
            {
                if(ctrigger->direction == 0 || ctrigger->direction == 2)
                    cenemyComp->currentDirection = ctrigger->direction;
            }
        }

        //Moving the enemy
        if(cenemyComp->currentDirection == 0)
        {
            events->emit<MoveEvent>(entity, MoveEvent::GoRight);
        }
        else if(cenemyComp->currentDirection == 2)
        {
            events->emit<MoveEvent>(entity, MoveEvent::GoLeft);
        }
    }
}
