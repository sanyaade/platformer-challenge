#include "RenderSystem.h"

#include <iostream>

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/View.hpp>
#include <SFML/Audio/Listener.hpp>

#include "../LevelState.h"
#include "../OptionsManager.h"
#include "../components/CFollowedByCamera.h"
#include "../components/CPolygon.h"
#include "../components/CBox.h"
#include "../components/CRender.h"
#include "../components/CZOrder.h"
#include "../events/PhysicEvent.h"
#include "../resources/Resources.h"

RenderSystem::RenderSystem(sf::RenderTarget &renderTarget, LevelSettings &levelSettings) :
    renderTarget(renderTarget),
    view(sf::FloatRect(0, 0, 1024, 768)),
    listOfEntities(*RenderSystem::CompareZOrder),
    settings(levelSettings)
{

}

void RenderSystem::configure(entityx::ptr<entityx::EventManager> eventManager)
{
    eventManager->subscribe<entityx::ComponentAddedEvent<CZOrder> >(*this);
    eventManager->subscribe<entityx::ComponentRemovedEvent<CZOrder> >(*this);
    eventManager->subscribe<PhysicEvent>(*this);
}

void RenderSystem::update(entityx::ptr<entityx::EntityManager> es, entityx::ptr<entityx::EventManager> events, double dt)
{
    //Update the view
    sf::Vector2i minPos;
    sf::Vector2i maxPos;
    int i = 0;
    for(auto entity : es->entities_with_components<CFollowedByCamera, CBox>())
    {
        entityx::ptr<CBox> posComp = entity.component<CBox>();

        if(i == 0)
        {
            minPos.x = maxPos.x = posComp->x;
            minPos.y = maxPos.y = posComp->y;
        }
        else
        {
            minPos.x = std::min(minPos.x, (int)posComp->x);
            minPos.y = std::min(minPos.y, (int)posComp->y);

            maxPos.x = std::max(maxPos.x, (int)posComp->x);
            maxPos.y = std::max(maxPos.y, (int)posComp->y);
        }

        i++;
    }

    sf::Vector2f center((minPos.x + maxPos.x)/2, (maxPos.y + minPos.y)/2);
    center.x = std::max(settings.levelBoundaries.left + view.getSize().x/2.f,
                        std::min((float)((minPos.x + maxPos.x)/2),
                                 settings.levelBoundaries.left + settings.levelBoundaries.width - view.getSize().x/2.f));
    center.y = std::max(settings.levelBoundaries.top + view.getSize().y/2.f,
                        std::min((float)((minPos.y + maxPos.y)/2),
                                 settings.levelBoundaries.top + settings.levelBoundaries.height - view.getSize().y/2.f));

    sf::Listener::setPosition(center.x, center.y, 0.f);
    view.setCenter(center);

    //Update animations
    for(auto it = listOfEntities.begin(); it != listOfEntities.end(); it++)
    {
        if(!(*it))
            continue;

        entityx::ptr<CRender> renderComp = const_cast<entityx::Entity&>(*it).component<CRender>();

        if(!renderComp || !renderComp->textureLoaded || !renderComp->animated)
            continue;

        renderComp->animator.update(sf::microseconds(static_cast<sf::Int64>(dt * 1000000)));
        renderComp->animator.animate(renderComp->sprite);
    }

    //Render
    sf::FloatRect viewRect(view.getCenter().x - view.getSize().x/2.f,
                           view.getCenter().y - view.getSize().y/2.f,
                           view.getSize().x,
                           view.getSize().y);

    renderTarget.setView(view);
    for(auto it = listOfEntities.begin(); it != listOfEntities.end(); it++)
    {
        if(!(*it))
            continue;

        entityx::ptr<CRender> renderComp = const_cast<entityx::Entity&>(*it).component<CRender>();
        entityx::ptr<CBox> posComp = const_cast<entityx::Entity&>(*it).component<CBox>();
        entityx::ptr<CZOrder> zComp = const_cast<entityx::Entity&>(*it).component<CZOrder>();

        if(!posComp || !renderComp || !zComp)
            continue;

        if(!renderComp->textureLoaded)
        {
            //Set the texture
            renderComp->textureLoaded = true;

            if(!Resources::Instance().GetTextureHolder().IsLoaded(renderComp->texturePath))
                Resources::Instance().GetTextureHolder().Load(renderComp->texturePath);

            renderComp->sprite.setTexture(Resources::Instance().GetTextureHolder().Get(renderComp->texturePath));
        }

        sf::FloatRect spriteRect(renderComp->sprite.getGlobalBounds());
        spriteRect.left += posComp->x;
        spriteRect.top += posComp->y;

        if(!spriteRect.intersects(viewRect))
        {
            // Out of the view, no need to draw this object
            continue;
        }

        sf::Transform state;
        state.translate((int)posComp->x, (int)posComp->y);

        renderTarget.draw(renderComp->sprite, state);
    }
    renderTarget.setView(sf::View(sf::FloatRect(0, 0, 1024, 768)));
}

void RenderSystem::receive(const entityx::ComponentAddedEvent<CZOrder> &event)
{
    listOfEntities.insert(event.entity);
}

void RenderSystem::receive(const entityx::ComponentRemovedEvent<CZOrder> &event)
{
    for(auto it = listOfEntities.begin(); it != listOfEntities.end(); it++)
    {
        if(!*it)
            continue;

        if(*it == event.entity)
        {
            listOfEntities.erase(it);
            return;
        }
    }
}

void RenderSystem::receive(const PhysicEvent &event)
{
    if(event.entity.component<CRender>())
    {
        std::string currentAnim(event.entity.component<CRender>()->defaultAnimation);
        CRender::Direction currentDir(event.entity.component<CRender>()->defaultDirection);

        if(event.entity.component<CRender>()->animator.isPlayingAnimation())
        {
            currentAnim = event.entity.component<CRender>()->animator.getPlayingAnimation().first;
            currentDir = event.entity.component<CRender>()->animator.getPlayingAnimation().second;
        }

        switch(event.eventType)
        {
        case PhysicEvent::Idle:
            currentAnim = "stand";
            break;
        case PhysicEvent::Walking:
            currentAnim = "walk";
            break;
        case PhysicEvent::Jump:
            currentAnim = "jump";
            break;
        case PhysicEvent::WatchingLeft:
            currentDir = CRender::Left;
            break;
        case PhysicEvent::WatchingRight:
            currentDir = CRender::Right;
            break;
        default:
            break;
        }

        if(event.entity.component<CRender>()->animator.isPlayingAnimation())
        {
            if(event.entity.component<CRender>()->animator.getPlayingAnimation().first != currentAnim ||
               event.entity.component<CRender>()->animator.getPlayingAnimation().second != currentDir)
                event.entity.component<CRender>()->animator.playAnimation(std::make_pair(currentAnim, currentDir),
                                                                          event.entity.component<CRender>()->animationLooping[std::make_pair(currentAnim, currentDir)]);
        }
        else
        {
            event.entity.component<CRender>()->animator.playAnimation(std::make_pair(currentAnim, currentDir),
                                                                      event.entity.component<CRender>()->animationLooping[std::make_pair(currentAnim, currentDir)]);
        }
    }
}

bool RenderSystem::CompareZOrder(entityx::Entity a, entityx::Entity b)
{
    if(!a || !b)
    {
        std::cout << "/!\\ Bad entities" << std::endl;
        return false;
    }

    entityx::ptr<CZOrder> renderA = a.component<CZOrder>();
    entityx::ptr<CZOrder> renderB = b.component<CZOrder>();

    if(!renderA || !renderB)
    {
        std::cout << "/!\\ Can't find CZOrder component" << std::endl;
        return false;
    }

    if(renderA->layer < renderB->layer)
    {
        return true;
    }
    else if(renderA->layer > renderB->layer)
    {
        return false;
    }
    else
    {
        return renderA->zOrder < renderB->zOrder;
    }
}
