#ifndef TEXTURECACHE_H
#define TEXTURECACHE_H

#include <iostream>
#include <map>

#include <SFML/Graphics/Texture.hpp>

#include "resources/Resources.h"
#include "Singleton.h"

class TextureCache : public Singleton<TextureCache>
{
    friend class Singleton<TextureCache>;

public:
    sf::Texture& GetTexture(const std::string &texturePath)
    {
        if(loadedTextures.count(texturePath) == 0)
        {
            sf::Texture newTexture;
            if(!newTexture.loadFromFile(texturePath))
            {
                std::cout << "TextureCache: Can't load " << texturePath << std::endl;
                return newTexture;
            }

            newTexture.setSmooth(false);

            std::cout << "TextureCache: Texture \"" << texturePath << "\" loaded." << std::endl;
            loadedTextures[texturePath] = newTexture;
            return loadedTextures[texturePath];
        }
        else
        {
            return loadedTextures[texturePath];
        }
    }

private:
    std::map<std::string, sf::Texture> loadedTextures;
};

#endif // TEXTURECACHE_H
