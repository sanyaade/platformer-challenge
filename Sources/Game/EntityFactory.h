#ifndef ENTITYFACTORY_H
#define ENTITYFACTORY_H

#include <map>
#include "entityx/entityx.h"
#include "yaml-cpp/yaml.h"

class EntityFactory
{
public:
    EntityFactory(entityx::ptr<entityx::EntityManager> entityManager);

    /// Create a new entity from a saved level (use a template + overload modified attributes according from the save)
    void CreateEntityFromSave(entityx::Entity &entity, YAML::Node node);

    /// Create a new entity from a template file
    void CreateEntityFromTemplate(entityx::Entity &entity, std::string templateFilePath, std::string templateName = "");
    void CreateEntityFromTemplate(entityx::Entity &entity, YAML::Node node);

private:
    void AssignComponentFromYAML(entityx::Entity &entity, YAML::Node compNode, bool resetIfExists = false);

    void AssignCBox(entityx::Entity &entity, YAML::Node &compNode, bool resetIfExists);
    void AssignCPolygon(entityx::Entity &entity, YAML::Node &compNode, bool resetIfExists);

    void AssignCZOrder(entityx::Entity &entity, YAML::Node &compNode, bool resetIfExists);
    void AssignCRender(entityx::Entity &entity, YAML::Node &compNode, bool resetIfExists);

    void AssignCPhysic(entityx::Entity &entity, YAML::Node &compNode, bool resetIfExists);
    void AssignCObstacle(entityx::Entity &entity, YAML::Node &compNode, bool resetIfExists);

    void AssignCPlayerSpawner(entityx::Entity &entity, YAML::Node &compNode, bool resetIfExists);
    void AssignCFinishLine(entityx::Entity &entity, YAML::Node &compNode, bool resetIfExists);

    void AssignCPlayer(entityx::Entity &entity, YAML::Node &compNode, bool resetIfExists);
    void AssignCEnemy(entityx::Entity &entity, YAML::Node &compNode, bool resetIfExists);
    void AssignCFollowedByCamera(entityx::Entity &entity, YAML::Node &compNode, bool resetIfExists);

    void AssignCLinearMovement(entityx::Entity &entity, YAML::Node &compNode, bool resetIfExists);
    void AssignCMovementTrigger(entityx::Entity &entity, YAML::Node &compNode, bool resetIfExists);

    void AssignCSoundSource(entityx::Entity &entity, YAML::Node &compNode, bool resetIfExists);

    entityx::ptr<entityx::EntityManager> entityManager;
    std::map<std::string, YAML::Node> loadedTemplates;
};

#endif // ENTITYFACTORY_H
