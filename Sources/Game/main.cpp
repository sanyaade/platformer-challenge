#include <iostream>
#include <sstream>

#include <SFML/Graphics.hpp>

#include "GameEngine.h"
#include "LevelState.h"
#include "MainMenu.h"
#include "PauseState.h"

#include "OptionsManager.h"
#include "polygon.h"
#include "Tools.h"

int main(int argc, char **argv)
{
    std::cout << "Starting Platformer Challenge... ";
    GameEngine engine;
    engine.Init(argc, argv);
    std::cout << "Started." << std::endl << std::endl;

    std::cout << "Registering all states..." << std::endl;
    engine.RegisterState<LevelState>();
    engine.RegisterState<PauseState>();
    engine.RegisterState<MainMenu>();
    std::cout << "Registered." << std::endl;

    std::cout << "Starting LevelState..." << std::endl;
    if(OptionsManager::Instance().Exists("level") && OptionsManager::Instance().Exists("players") && OptionsManager::Instance().GetOptionAsString("level", "") != "")
        engine.ChangeState<LevelState>();
    else
        engine.ChangeState<MainMenu>();
    std::cout << "Started." << std::endl;

    return engine.Run();
}
