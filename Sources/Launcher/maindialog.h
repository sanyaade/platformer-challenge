#ifndef MAINDIALOG_H
#define MAINDIALOG_H

#include <QDialog>
#include <QComboBox>
#include <QFileSystemModel>

namespace Ui {
class MainDialog;
}

class MainDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MainDialog(QWidget *parent = 0);
    ~MainDialog();

private slots:
    void on_playLevelBt_toggled(bool checked);

    void on_browseBt_clicked();

    void on_toolButton_clicked();

    void on_quitBt_clicked();

    void on_editorBt_clicked();

private:
    void InsertPlayerList(QComboBox *combo, bool optionalPlayer = false);

    QString GenerateCommandLine();

    Ui::MainDialog *ui;
};

#endif // MAINDIALOG_H
